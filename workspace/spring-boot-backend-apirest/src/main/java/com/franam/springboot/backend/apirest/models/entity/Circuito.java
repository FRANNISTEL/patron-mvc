package com.franam.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "circuito")
public class Circuito implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	@Size(min = 3, max = 20)
	@Column(nullable = false, unique = true)
	private String nombre;

	@NotEmpty
	@Size(min = 3, max = 20)
	@Column(nullable = false)
	private String ciudad;

	@NotEmpty
	@Size(min = 3, max = 20)
	@Column(nullable = false)
	private String pais;

	@Range(min = 40, max = 80, message = "debe estar entre 40 y 80")
	@Column(nullable = false)
	private int vueltas;

	@Range(min = 3000, max = 9000, message = "debe estar entre 3000 y 9000")
	@Column(nullable = false)
	private int longitud;

	@Range(min = 6, max = 20, message = "debe estar entre 6 y 20")
	@Column(nullable = false)
	private int curvas;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public int getVueltas() {
		return vueltas;
	}

	public void setVueltas(int vueltas) {
		this.vueltas = vueltas;
	}

	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public int getCurvas() {
		return curvas;
	}

	public void setCurvas(int curvas) {
		this.curvas = curvas;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
