package com.franam.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.franam.springboot.backend.apirest.models.entity.Circuito;

public interface ICircuitoService {

	public List<Circuito> findAll();

	public Page<Circuito> findAll(Pageable pageable);

	public Circuito save(Circuito circuito);

	public void delete(Long id);

	public Circuito findById(Long id);
}
