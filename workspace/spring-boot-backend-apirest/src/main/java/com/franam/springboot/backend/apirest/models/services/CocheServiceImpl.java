package com.franam.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.franam.springboot.backend.apirest.models.dao.ICocheDao;
import com.franam.springboot.backend.apirest.models.entity.Coche;

@Service
public class CocheServiceImpl implements ICocheService {

	@Autowired
	private ICocheDao cocheDao;

	@Override
	@Transactional(readOnly = true)
	public List<Coche> findAll() {
		return (List<Coche>) cocheDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Coche> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return cocheDao.findAll(pageable);
	}

	@Override
	public Coche save(Coche coche) {
		return cocheDao.save(coche);
	}

	@Override
	public void delete(Long id) {
		cocheDao.deleteById(id);

	}

	@Override
	public Coche findById(Long id) {
		return cocheDao.findById(id).orElse(null);
	}

}
