package com.franam.springboot.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.franam.springboot.backend.apirest.models.entity.Resultado;

public interface IResultadoDao extends JpaRepository<Resultado, Long> {

}
