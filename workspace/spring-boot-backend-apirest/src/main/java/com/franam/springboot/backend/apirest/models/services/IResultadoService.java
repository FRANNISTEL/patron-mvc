package com.franam.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.franam.springboot.backend.apirest.models.entity.Resultado;

public interface IResultadoService {

	public List<Resultado> findAll();

	public Page<Resultado> findAll(Pageable pageable);

	public Resultado save(Resultado resultado);

	public void delete(Long id);

	public Resultado findById(Long id);

	public Resultado calcularTotal(Resultado resultado);

}
