package com.franam.springboot.backend.apirest.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franam.springboot.backend.apirest.models.entity.Resultado;
import com.franam.springboot.backend.apirest.models.services.IResultadoService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class ResultadoRestController {

	@Autowired
	private IResultadoService resultadoService;

	@GetMapping("/resultados")
	public List<Resultado> index() {
		return resultadoService.findAll();
	}

	@GetMapping("/resultados/page/{page}")
	public Page<Resultado> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4);
		return resultadoService.findAll(pageable);

	}

	@GetMapping("/resultados/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		Resultado resultado = null;
		Map<String, Object> response = new HashMap<>();

		try {
			resultado = resultadoService.findById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (resultado == null) {
			response.put("mensaje", "El resultado Id: ".concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Resultado>(resultado, HttpStatus.OK);
	}

	@PostMapping("/resultados")
	public ResponseEntity<?> create(@Valid @RequestBody Resultado resultado, BindingResult result) {

		Resultado resultadoNew = null;
		Map<String, Object> response = new HashMap<>();
		System.out.println(resultado.getCoche().getNombre());
		System.out.println(resultado.getCircuito().getNombre());
		resultadoService.calcularTotal(resultado);
		System.out.println(resultado.getTotal());

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			resultadoNew = resultadoService.save(resultado);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El resultado ha sido creado con exito!");
		response.put("resultado", resultadoNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@PutMapping("/resultados/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Resultado resultado, BindingResult result,
			@PathVariable Long id) {

		Resultado resultadoActual = resultadoService.findById(id);
		Resultado resultadoUpdate = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (resultadoActual == null) {
			response.put("mensaje", "Error: no se puede editar, el resultado Id: "
					.concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		try {
			resultadoActual.setCoche(resultado.getCoche());
			resultadoActual.setCircuito(resultado.getCircuito());
			resultadoUpdate = resultadoService.save(resultadoActual);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El resultado ha sido actualizado con exito!");
		response.put("resultado", resultadoUpdate);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@DeleteMapping("/resultados/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			resultadoService.delete(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El resultado ha sido eliminado con exito!");

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

}
