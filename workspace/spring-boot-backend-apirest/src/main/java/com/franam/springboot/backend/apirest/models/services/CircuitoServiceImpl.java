package com.franam.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.franam.springboot.backend.apirest.models.dao.ICircuitoDao;
import com.franam.springboot.backend.apirest.models.entity.Circuito;

@Service
public class CircuitoServiceImpl implements ICircuitoService {

	@Autowired
	private ICircuitoDao circuitoDao;

	@Override
	@Transactional(readOnly = true)
	public List<Circuito> findAll() {
		return (List<Circuito>) circuitoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Circuito> findAll(Pageable pageable) {
		return circuitoDao.findAll(pageable);
	}

	@Override
	public Circuito save(Circuito circuito) {
		return circuitoDao.save(circuito);
	}

	@Override
	public void delete(Long id) {
		circuitoDao.deleteById(id);
	}

	@Override
	public Circuito findById(Long id) {
		return circuitoDao.findById(id).orElse(null);
	}

}
