package com.franam.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.franam.springboot.backend.apirest.models.entity.Coche;

public interface ICocheService {

	public List<Coche> findAll();

	public Page<Coche> findAll(Pageable pageable);

	public Coche save(Coche coche);

	public void delete(Long id);

	public Coche findById(Long id);

}
