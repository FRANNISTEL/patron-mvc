package com.franam.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.franam.springboot.backend.apirest.models.dao.IResultadoDao;
import com.franam.springboot.backend.apirest.models.entity.Resultado;

@Service
public class ResultadoServiceImpl implements IResultadoService {

	@Autowired
	private IResultadoDao resultadoDao;

	@Override
	@Transactional(readOnly = true)
	public List<Resultado> findAll() {
		return (List<Resultado>) resultadoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Resultado> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return resultadoDao.findAll(pageable);
	}

	@Override
	public Resultado save(Resultado resultado) {
		return resultadoDao.save(resultado);
	}

	@Override
	public void delete(Long id) {
		resultadoDao.deleteById(id);

	}

	@Override
	public Resultado findById(Long id) {
		return resultadoDao.findById(id).orElse(null);
	}

	@Override
	public Resultado calcularTotal(Resultado resultado) {

		double total = resultado.getCircuito().getCurvas() * resultado.getCircuito().getVueltas()
				* resultado.getCoche().getGanancia();
		resultado.setTotal(total);
		return resultado;
	}

}
